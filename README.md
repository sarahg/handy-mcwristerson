# handy-mcwristerson

I am a Slack bot that recommends a hand or wrist stretch every hour.

## How it works

- On the hour, the CI job in this project runs `index.js`
- `index.js` picks a random exercise from `exercises.json`
- `index.js` then posts content to Slack using [Bolt](https://slack.dev/bolt-js/concepts)

## Add content

1. Use Slack's [Block Kit Builder](https://app.slack.com/block-kit-builder/TNKSSD2F6#%7B%22blocks%22:%5B%5D%7D) to create a post.
2. Add the full copy/paste of that block as a new object in `exercises.json`.

## Development

1. Clone this project and install dependencies with `npm install`.
2. Copy the `SLACK_SIGNING_SECRET` and `SLACK_BOT_TOKEN` values from the app's settings on Slack.
3. Run the script locally:

```
SLACK_CHANNEL="general" SLACK_SIGNING_SECRET="abc" SLACK_BOT_TOKEN="xoxb-def" ./index.js
```