#!/usr/bin/env node

const exercises = require("./content/exercises.json");
const { App } = require("@slack/bolt");

// @todo be smarter
// const POST_START_HOUR = 14; // 14:00 UTC = 9:00AM Central Daylight Time
const POST_START_HOUR = 15; // 15:00 UTC = 9:00AM Central Standard Time

const app = new App({
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  token: process.env.SLACK_BOT_TOKEN,
});

(async () => {
  try {
    let index = 0;

    /**
     * Post exercises in sequence, based on the CI job timestamp.
     * If we don't have a timestamp, just pick a random one.
     */
    if (process.env.CI_JOB_STARTED_AT) {
      const date = new Date(process.env.CI_JOB_STARTED_AT);
      const hour = date.getUTCHours();
      index = hour - POST_START_HOUR;
    } else {
      index = Math.floor(Math.random() * exercises.length);
    }

    // Post to Slack
    const result = await app.client.chat.postMessage({
      channel: process.env.SLACK_CHANNEL,
      text: "Time to stretch your hands and wrists!",
      blocks: exercises[index].blocks,
    });

    console.log(result);
  } catch (error) {
    console.error(error);
  }
})();
